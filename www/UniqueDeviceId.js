var exec = require('cordova/exec');

exports.getUniqueDeviceId = function (arg0, success, error) {
    exec(success, error, 'UniqueDeviceId', 'getUniqueDeviceId', [arg0]);
};
