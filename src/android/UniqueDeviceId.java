package cordova.plugin.uniquedeviceid;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.Build;

/**
 * This class echoes a string called from JavaScript.
 */
public class UniqueDeviceId extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("getUniqueDeviceId")) {
            String message = args.getString(0);
            this.getDeviceId(message, callbackContext);
            return true;
        }
        return false;
    }

    private void getDeviceId(String message, CallbackContext callbackContext) {
        String deviceId = "35" + // we make this look like a valid IMEI
                Build.BOARD.length() % 10 + Build.BRAND.length() % 10 + Build.CPU_ABI.length() % 10
                + Build.DEVICE.length() % 10 + Build.DISPLAY.length() % 10 + Build.HOST.length() % 10
                + Build.ID.length() % 10 + Build.MANUFACTURER.length() % 10 + Build.MODEL.length() % 10
                + Build.PRODUCT.length() % 10 + Build.TAGS.length() % 10 + Build.TYPE.length() % 10
                + Build.USER.length() % 10; // 13 digits
        if (deviceId != null && deviceId.length() > 0) {
            callbackContext.success(deviceId);
        } else {
            callbackContext.error("Something went wrong!");
        }
    }
}
